'use strict';



module.exports = class ContextError extends Error {
	constructor(...args) {
		super(...args);
		this.name = 'ContextError';
	}
}
