# @bitaffair/ambix

Immutable context objects.


# Install

`npm install --save @bitaffair/ambix`

# Example

```js
const { createContext } = require('@bitaffair/ambix');

const ctx = createCtx({
  project: 'p1',
  user: 'u1'
});
```

## `createContext(raw)`

Returns an immuatble `context`.

## `context.$extend(raw)`

Returns a new immutable `context` which inherits from parent.

## `context.$parent`

Returns parent `context`.

## `context.$validate(schema)`

Asserts current `context` with given Joi-schema.


## `context.toObject()`

Creates raw object from current `context` values.



### Neologism `ambix`

Fusion of `ambit` and `context`.
