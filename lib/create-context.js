'use strict';


const Joi = require('joi');

const { ContextError, ContextExtendError, ContextValidationError } = require('./errors');

const cloneObj = obj => JSON.parse(JSON.stringify(obj));




module.exports = function createCtx(raw, parent = {}) {

	if (!raw) {
		throw new ContextError('Missing input');
	}

	if (typeof raw !== 'object' || Array.isArray(raw) || raw === null) {
		throw new ContextError('Invalid input');
	}


	const ctx = {
		...parent
	};

	for (let [key, val] of Object.entries(raw)) {
		if (key in parent) {
			throw new ContextExtendError(`Key '${key}' already exists on parent context.`);
		}
		ctx[key] = val;
	}


	const makeProxySetError = (ctx, key, val) => {
		let errorMessage = `Forbidden to set context.${key} = ${val}. Context is always immutable!`;

		if (!(key in ctx)) {
			errorMessage += ` Use context.$extend({ ${key}: ${val} }) to create a child.`;
		}

		throw new ContextError(errorMessage);
	}

	const proxy = new Proxy(ctx, {
		get(ctx, key) {
			if (key in ctx) {
				return ctx[key];
			}

			throw new ContextError(`context.${key} is not defined`);
		},
		set(ctx, key, val) {
			throw makeProxySetError(ctx, key, val);
		},
		defineProperty(ctx, key, val) {
			throw makeProxySetError(ctx, key, val);
		}
	});


	Object.defineProperty(ctx, 'toObject', {
		value: () => {
			return cloneObj(ctx);
		}
	});

	Object.defineProperty(ctx, '$parent', {
		value: parent
	});

	Object.defineProperty(ctx, '$extend', {
		value: (raw) => {
			return createCtx(raw, proxy)
		}
	});

	Object.defineProperty(ctx, '$validate', {
		value: (schema) => {

			try {
				Joi.attempt(proxy, Joi.object(schema).options({ allowUnknown: true }))
			} catch (e) {
				throw new ContextValidationError(e.message);
			}

		}
	});


	return proxy;

}
