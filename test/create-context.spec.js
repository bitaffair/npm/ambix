'use strict';




const { expect } = require('@hapi/code');
const { createContext, errors: { ContextError, ContextExtendError, ContextValidationError } } = require('../');



describe('createContext()', () => {

	it('should throw Error on missing input', () => {

		expect(() => createContext()).to.throw(ContextError, 'Missing input');

	});

	it('should throw ContextError on non-object input', () => {

		expect(() => createContext('test'), 'string').to.throw(ContextError, 'Invalid input');
		expect(() => createContext(123), 'number').to.throw(ContextError, 'Invalid input');
		expect(() => createContext([]), 'array').to.throw(ContextError, 'Invalid input');

	});



	describe('context', () => {

		let ctx;
		before(() => {
			ctx = createContext({
				project: 'p1',
				user: 'u1'
			});
		});



		describe('getting values', () => {

			it('should get value', () => {

				expect(ctx.project).to.equal('p1');
				expect(ctx.user).to.equal('u1');

			});

			it('should throw on missing value', () => {
				expect(() => ctx.unknownValue).to.throw(ContextError, 'context.unknownValue is not defined');
			});

		});



		describe('setting values', () => {

			it('should throw on defined keys', () => {

				expect(() => ctx.project = 'p2').to.throw(ContextError, 'Forbidden to set context.project = p2. Context is always immutable!')

			});

			it('should throw on undefined keys', () => {
				expect(() => ctx.undefinedValue = 'test').to.throw(ContextError);
			});

			it('should throw on Object.defineProperty(context, ...)', () => {

				expect(() => {
					Object.defineProperty(ctx, 'undefinedValue', { value: 'test' });
				}).to.throw(ContextError)

			});
		});



		describe('toObject()', () => {

			it('should get values', () => {

				expect(ctx.toObject()).to.equal({
					project: 'p1',
					user: 'u1'
				});

			});

		});



		describe('$extend()', () => {

			it('should throw on already defined attribute on parent context', () => {

				expect(() => ctx.$extend({
					project: 'p2'
				})).to.throw(ContextExtendError, `Key 'project' already exists on parent context.`)

			});

			it('should create new context inherited from parent', () => {


				const child = ctx.$extend({
					task: 't1'
				});

				expect(child.project).to.equal('p1');
				expect(child.user).to.equal('u1');
				expect(child.task).to.equal('t1');

				expect(ctx.toObject()).only.includes(['project', 'user']);
			});

		});



		describe('$parent', () => {

			it('should give access to parent context', () => {

				const child = ctx.$extend({ task: 't1' });

				expect(child.$parent === ctx).to.be.true();

			});

		});



	});
});
