'use strict';




module.exports = class ContextValidationError extends Error {
	constructor(...args) {
		super(...args);
		this.name = 'ContextValidationError';
	}
}
