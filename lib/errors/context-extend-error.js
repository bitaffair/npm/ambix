'use strict';


module.exports = class ContextExtendError extends Error {
	constructor(...args) {
		super(...args);
		this.name = 'ContextExtendError';
	}
}
